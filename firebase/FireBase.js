const firebase =  require("firebase");
const firebaseConfig = require("./secret/firebaseConfig");
firebase.initializeApp(firebaseConfig);

module.exports = firebase.database();
