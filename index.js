const getHTML = require('html-get');
const jsdom = require("jsdom");
const {toJSON} = require("./utils/dom-to-json");
const html2json = require('html2json').html2json;
const db = require("./firebase/FireBase");

const settings = {
	url: 'https://oxmedia.pl',
};

const _fetchDOM = async (url) => {
	try {
		const DOM = await getHTML(url);
		return DOM.html;
	} catch (e) {
		console.error(e);
	}
};

const processDOM = async (url) => {
	const DOM = await _fetchDOM(url);
	let jsonResponse = [];
	if (DOM) {
		const {JSDOM} = jsdom;
		const browser = new JSDOM(DOM);

		const slides = browser.window.document.querySelectorAll('.slide');

		let videoVertical, jsonOutput, html;
		slides.forEach((slide, index) => {
			/**
			 * Get Video and Images
			 */
			videoVertical = slide.querySelector('video');
			if (videoVertical) {
				(videoVertical.hasChildNodes()) ? videoVertical = videoVertical.children['0'].dataset.src : videoVertical = '';
			}

			/**
			 * Get nodes with content
			 */

			if (slide.querySelector('[class*="__content"]') !== null) {
				jsonOutput = html2json(slide.querySelector('[class*="__content"]').innerHTML);
			} else {
				jsonOutput = null
			}


			jsonResponse[index] = {
				id: index,
				name: slide.dataset.title,
				className: slide.classList,
				temlpate: null,
				background: {
					vertical: {
						video: videoVertical
					}
				},
				child: jsonOutput
			};
		})
	}

	return jsonResponse;
};

processDOM(settings.url).then((resp) => {
	console.log(JSON.stringify(resp, null, 4));
	// writeSlidesData(resp);
});


function writeUserData(slides) {
	db.ref('pages/home').set({
		slides,
	});
}
